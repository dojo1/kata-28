require_relative 'split_hash_by_key'
require 'test/unit'

class SplitHashByKeyTest < Test::Unit::TestCase
  def test_case_1
    input =  { :a=>1, :b=>2, :c=>3, :d=>4, :e=>5, :f=>6 }
    assert_equal [ {:a=>1, :b=>2}, {:c=>3, :d=>4}, {:e=>5, :f=>6} ], SplitHash.new(input, :c, :e).execute
  end

  def test_case_2
    input =  { :a=>1, :b=>2, :c=>3, :d=>4, :e=>5, :f=>6 }
    assert_equal [ {:a=>1}, {:b=>2, :c=>3, :d=>4, :e=>5}, {:f=>6} ], SplitHash.new(input, :b, :f).execute
  end

  def test_case_3
    input =  { 'a'=>1, 'b'=>2, 'c'=>3, 'd'=>4, 'e'=>5, 'f'=>6 }
    assert_equal [ {"a"=>1, "b"=>2, "c"=>3}, {"d"=>4, "e"=>5, "f"=>6} ], SplitHash.new(input, 'd').execute
  end

  def test_case_4
    input = {:a => 1, :b => 2}
    assert_equal [ {:a => 1, :b => 2} ], SplitHash.new(input, :a).execute
  end

  def test_case_5
    input = {:a => 1, :b => nil, :c => ''}
    assert_equal [ {:a => 1} ], SplitHash.new(input, :a).execute
  end

  def test_case_6
    input = { :a=>1, :b=>2, :c=>3, :d=>4, :e=>5, :f=>6 }
    assert_raises(Exception) { SplitHash.new(input, 'b').execute }
  end
end
