require 'active_support/core_ext'

class SplitHash
  attr_reader :input, :args, :result

  def initialize(input, *args)
    @input = input
    @args = *args
    @result = []
  end

  def execute
    raise Exception if no_args_present?

    input.each { |key, value| assign_entry(key, value) }

    result
  end

  private

  def no_args_present?
    args.map do |arg|
      input.has_key?(arg)
    end.none?
  end

  def assign_entry(key, value)
    return if value.blank?

    result << {} if should_create_new_hash?(key)
    result.last[key] = value
  end

  def should_create_new_hash?(key)
    result.empty? || args.include?(key)
  end
end
